<?php
/**
 * The sidebar containing the off canvas widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package marinate
 */
?>
		 <!-- Pushy Sidebar -->
        <nav class="pushy pushy-left">
            <div class="pushy-content">
                <div class="widget-area" role="complementary">					
                
                
						<?php
                            $marinate_socialheader = get_theme_mod('marinate_social_ed', '0');
                            $marinate_fb = get_theme_mod('marinate_button_url_fb');
                            $marinate_tw = get_theme_mod('marinate_button_url_tw');
                            $marinate_pin = get_theme_mod('marinate_button_url_pin');
                            $marinate_insta = get_theme_mod('marinate_button_url_ins');
                            $marinate_gplus = get_theme_mod('marinate_button_url_gp');
                            if ($marinate_socialheader) {
                            ?>
	                		<aside class="widget widget_social_menu hidden-md hidden-lg">                            
                            <ul class="social-networks">
                                <?php if ($marinate_fb) { ?><li><a class="facebook" href="<?php echo esc_url($marinate_fb); ?>"><i class="fa fa-facebook"></i></a></li><?php } ?>
                                <?php if ($marinate_tw) { ?><li><a class="twitter" href="<?php echo esc_url($marinate_tw); ?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
                                <?php if ($marinate_pin) { ?><li><a class="pinterest" href="<?php echo esc_url($marinate_pin); ?>"><i class="fa fa-pinterest-p"></i></a></li><?php } ?>
                                <?php if ($marinate_insta) { ?><li><a class="instagram" href="<?php echo esc_url($marinate_insta); ?>"><i class="fa fa-instagram"></i></a></li><?php } ?>
                                <?php if ($marinate_gplus) { ?><li><a class="google-plus" href="<?php echo esc_url($marinate_gplus); ?>"><i class="fa fa-google-plus"></i></a></li><?php } ?>
                            </ul>
	                        </aside>                            
                            <?php
                            }
                        ?>
                                                        
                		<!-- <aside class="widget widget_primary_menu hidden-md hidden-lg">
						  <?php
                            // wp_nav_menu( array(
							// 	'theme_location' => 'menu-1',
							// 	'menu_id'        => '',
							// 	'container'      => 'ul',
							// 	'fallback_cb'    => 'wp_page_menu',
							// 	'menu_class'     => '',
							// 	'item_wrap'      => '<ul class="%2$s">%3s</ul>',						  
                            // ) );
                          ?>                         
                        </aside> -->
                
                        <?php dynamic_sidebar( 'sidebar-1' ); ?>
                </div>
            </div>
        </nav>