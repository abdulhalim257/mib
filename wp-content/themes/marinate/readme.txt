=== Marinate ===
Contributors: metricthemes
Tags: custom-menu, two-columns, grid-layout, right-sidebar, custom-logo, featured-images, full-width-template, custom-header, featured-image-header, flexible-header, sticky-post, custom-background, footer-widgets, threaded-comments, translation-ready, theme-options, blog, photography, food-and-drink
Requires at least: 4.9.6
Tested up to: 5.5
Requires PHP: 5.6
Stable tag: 1.0.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Marinate WordPress theme is a perfect solution if you want to start a food blog or a photography blog or just want to show case your portoflio.

== Description ==
Marinate WordPress theme is a perfect solution if you want to start a food blog or a photography blog or just want to show case your portoflio. Marinate comes with grid like layout for its Homepage and full width featured image for single posts which is perfect to showcase your high-res photos. Marinate also has an inbuilt off canvas widget area which works on desktop, mobile alike. Marinate is best theme for personal, lifestyle, food, travel, fashion, corporate, photography and any other kind of amazing blogs. It is very easy to use even for WordPress beginners. Clean, modern and responsive, marinate is perfect to showcase your content on any device.  

Theme demo: http://preview.metricthemes.com/marinate/

== Installation ==
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Custom Feature Documentation ==
To add Social Media Links in Header
1. Go to Dashboard >> Appereance >> Customie
2. Click on Social Media Settings Panel
3. Select Checkbox "Enable Social Links in Header" to enable Social Media links in Header
4. Select Checkbox "Enable Social Links in Footer" to enable Social Media links in Footer
5. Enter URLS for your social media profiles.  

== Changelog ==

= 1.0.0 = 
* Initital Release

= 1.0.1 = 
* Fixed Container width and added custom columns in archive pages.

= 1.0.2 = 
* Fixed post parkup issues

= 1.0.3 = 
* Fixed comments page and added welcome page

= 1.0.4 = 
* Fixed image not centering issue.

= 1.0.5 = 
* Fixed Script error issue after updating to WP 5.0
* Updated readme.txt format

= 1.0.6 = 
* Fixed broken theme link
* Updated readme.txt

= 1.0.7 = 
* Fixed skip links
* Fixed keyboard navigation for main menu
* Fixed admin notification in dashboard.

= 1.0.8 = 
* Fixed search display empty on mobile.

= 1.0.9 = 
* Fixed date not showing in blogposts.
* Updates screenshot images

== Copyrights and License ==

Unless otherwise specified, all the theme files, scripts and images are licensed under GPLv2 or later

== Resources ==
* Muli by Vernon Adams through Google Font - SIL Open Font License, 1.1
* Nunito Sans Font by Google Corporation through Google Font - SIL Open Font License, 1.1
* Font Awesome
	**Font License Applies to all desktop and webfont files in the following directory: marinate/fonts/. License: SIL OFL 1.1
	**Code License Applies to all CSS in the following file: marinate/css/font-awesome.css. License: MIT License
* Underscores (C) 2012-2016 Automattic, Inc. License: [GPLv2 or later]
* normalize.css (C) 2012-2016 Automattic, Inc. License: (C) 2012-2015 Nicolas Gallagher and Jonathan Neal
* bootstrap.css License: (C) 2011-2017 Mark Otto and Jacob Thornton
* Pushy Copyright (c) 2018 Christopher Yee License: MIT License

URL of Image used in screenshot:
* https://pxhere.com/en/photo/559132 - CC0 Creative Commons
* https://pxhere.com/en/photo/785294 - CC0 Creative Commons
* https://pxhere.com/en/photo/1585 - CC0 Creative Commons
* https://pxhere.com/en/photo/1002096 - CC0 Creative Commons
* https://pxhere.com/en/photo/895058- CC0 Creative Commons
* https://pxhere.com/en/photo/1588071 - CC0 Creative Commons

All other resources and theme elements are licensed under the GPLv2 or later

Marinate WordPress Theme, Copyright MetricThemes 2018, MetricThemes.com
Marinate WordPress Theme is distributed under the terms of the GPLv2 or later


*  This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.