<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'MIB' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'HOamdRRCQf3lj3Pjv48ZtsTracW3zn29sCh5O8X6RZmYWUDLTf2qHmrIptCsEtgT' );
define( 'SECURE_AUTH_KEY',  'Cx2PY3gQrEIFmd8skBEFsxKkfIBxkWhuayaFp187IkCMX5xsbQHKZy52OWdsw5Jx' );
define( 'LOGGED_IN_KEY',    'mVCECrx2Q9LCK5qLHLYCChzfkvw0UJB73boHBrFZMPKZt5jWRSLPlnFdikzybIYJ' );
define( 'NONCE_KEY',        'Z6xZgnoTxCMsAgEmCC8iUzsEd4cnzu0wBGpClLKcs6Z65ogTHs7t663EMeWRXxev' );
define( 'AUTH_SALT',        'QHDzZX6rKIfFKZWeXnf5hSYciEhfW8qCHbpHOPIQn0S2Avsc8xf3J0kzyG8yqOiZ' );
define( 'SECURE_AUTH_SALT', '8hQ11NLPDaU37CETqZSzhLa7h1RjIqh73meVBBKre8ETPh4kb6qScrqQQwMa8RXo' );
define( 'LOGGED_IN_SALT',   'zfGyTXtYotBk792SJTY7sle6isVXoOgCo9ttzwD7nLpKsvIbBjSyZIXSzRSAKr40' );
define( 'NONCE_SALT',       'GwQ6DiW4Qxo2Yem3DkiJlm1g94pIAuyWY3XQy4eAp3L3aQHSH2sNApRFKBj2RWyS' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
